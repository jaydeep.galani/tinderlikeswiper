import React, { Component } from 'react';
import { Text, View, SafeAreaView } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from './src/components/Home';
import Profile from './src/components/Profile';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducersAll from './src/reducers';
import Favorites from './src/components/Favorites';

const store = createStore(reducersAll, {}, applyMiddleware(ReduxThunk));

const Stack = createStackNavigator();

export default class App extends Component {
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Provider store={store}>
          <NavigationContainer>
            <Stack.Navigator headerMode={'none'}>
              <Stack.Screen name={'Profile'} component={Profile} />
              <Stack.Screen name={'Favorites'} component={Favorites} />
              <Stack.Screen name={'Home'} component={Home} />
            </Stack.Navigator>
          </NavigationContainer>
        </Provider>
      </SafeAreaView>
    );
  }
}
