import {PEOPLE_TYPE} from '../actions/Types';

const INITIAL_STATE = {
  peopleData: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PEOPLE_TYPE.RANDOM_DATA:
      console.log('===========reducer==========');
      // console.log(action.payload);
      peopleData = action.payload.data;

      return {...state, peopleData};

    default:
      return {...state};
  }
};
