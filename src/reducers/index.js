import {combineReducers} from 'redux';
import HomeReducer from './HomeReducer';
import {LOGIN_TYPE} from '../actions/Types';

const appReducer = combineReducers({
  HomeReducer: HomeReducer,
});

export default rootReducer = (state, action) => {
  return appReducer(state, action);
};
