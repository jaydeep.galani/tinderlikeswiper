import axios from 'axios';
import {API_TIMEOUT} from './Constants';
import {BASE_URL} from './URL';

var axiosApi = axios.create({
  baseURL: BASE_URL,
});

const configHeader = () => {
  return (header = {
    headers: {
      'Content-Type': 'application/json',
    },
    timeout: API_TIMEOUT,
  });
};

export const getRequestApi = (URL, dispatch, actionType) => {
  let headers = configHeader();
  console.log('url', URL);
  return axiosApi
    .get(URL, headers)
    // .then(res => JSON.parse(res))
    .then(response => {
      dispatch({type: actionType, payload: response});
    })
    .catch(error => {
      console.log(JSON.stringify(error));
    });
};
