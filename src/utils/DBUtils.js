import AsyncStorage from '@react-native-community/async-storage';

export const DBSchemaName = 'FAVORITES';

export const storeDataInDB = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (e) {
    // saving error
  }
};

export const getDataFromDB = async key => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      return value;
    }
  } catch (e) {
    return null;
    // error reading value
  }
};

export const removeValueFromDB = async key => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (e) {
    // remove error
  }
};

export const addProfile = async data => {
  console.log('data add =', data);
  let newData = await getAllProfiles();
  console.log('data before add', newData);
  newData = [data, ...newData];
  console.log('data after add', newData);
  newData = JSON.stringify(newData);
  await storeDataInDB(DBSchemaName, newData);
};

export const getAllProfiles = async () => {
  let newData = await getDataFromDB(DBSchemaName);
  console.log('new data================================', newData);
  if (newData) {
    newData = JSON.parse(newData);
  } else {
    newData = [];
  }
  return newData;
};

export const removeProfile = async md5 => {
  let data = await getAllProfiles();
  let newData = data.filter(item => {
    if (item.md5 != md5) return item;
  });
  newData = JSON.stringify(newData);
  await storeDataInDB(DBSchemaName, newData);
};

export const removeAllProfiles = async () => {
  await AsyncStorage.clear();
};
