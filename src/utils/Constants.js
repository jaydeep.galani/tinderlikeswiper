export const API_TIMEOUT = 30000;

export const COLORS = {
  backgroundDefault: '#F5FCFF',
  backgroundProfile: '#2c2e31',
  lightBackground: '#FFFFFF',
  lightTextColor: '#FFFFFF',
  danger: '#FF0000',
  success: '#008000',
  infoText: '#808080',
  cardBack: '#F1F0EF',
};
