// import {getRequestApi} from '../utils/ServiceManager';
import {PEOPLE_TYPE} from './Types';
import {API_PEOPLE} from '../utils/URL';
import { getRequestApi } from '../utils/ServiceManager';

export const randomPeopleDataAction = () => {
  console.log('===========randomPeopleDataAction=======');
  return dispatch =>
    getRequestApi(API_PEOPLE, dispatch, PEOPLE_TYPE.RANDOM_DATA);
};
