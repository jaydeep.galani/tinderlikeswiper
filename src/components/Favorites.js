import React, { Component } from 'react';
import Swiper from 'react-native-deck-swiper';
import { StyleSheet, Text, View, Image, StatusBar } from 'react-native';
import Card from './Card';
import { getAllProfiles, removeProfile } from '../utils/DBUtils';
import { COLORS } from '../utils/Constants';
class Favorites extends Component {
  constructor(props) {
    super(props);
    this.state = {
      peopleData: [],
      swipedAllCards: false,
      swipeDirection: '',
      cardIndex: 0,
      isFirstDataFetched: false,
      swiperKey: 0,
    };
  }
  componentDidMount = async () => {
    let peopleData = await getAllProfiles();

    this.setState({
      peopleData,
      swiperKey: this.state.swiperKey + 1,
      isFirstDataFetched: true,
      swipedAllCards: peopleData.length == 0,
    });
  };
  renderCard = (card, index) => {
    return <Card card={card} index={index} />;
  };

  onSwiped = type => {
    if (type == 'left') {
      let data = this.state.peopleData[this.state.cardIndex];
      removeProfile(data.md5);
    }
    this.setState({ cardIndex: this.state.cardIndex + 1 });
  };

  onSwipedAllCards = () => {
    this.setState({
      swipedAllCards: true,
    });
  };

  render() {
    return (
      <View key={this.state.swiperKey} style={styles.container}>
        <StatusBar backgroundColor={COLORS.backgroundProfile} />
        <View style={styles.upperBackground}>
          <Text style={styles.headerText}>{'Favorites'}</Text>
        </View>
        <View style={styles.swiperWrapper}>
          {this.state.isFirstDataFetched ? (
            this.state.swipedAllCards ? (
              <View style={styles.infoTextWrapper}>
                <Text style={styles.infoText}>
                  {'No more Favorites are there...'}
                </Text>
              </View>
            ) : (
                <Swiper
                  ref={swiper => {
                    this.swiper = swiper;
                  }}
                  backgroundColor={'transparent'}
                  onSwiped={() => this.onSwiped('general')}
                  onSwipedLeft={() => this.onSwiped('left')}
                  onSwipedRight={() => this.onSwiped('right')}
                  onSwipedTop={() => this.onSwiped('top')}
                  onSwipedBottom={() => this.onSwiped('bottom')}
                  cards={this.state.peopleData}
                  cardIndex={this.state.cardIndex}
                  cardVerticalMargin={50}
                  renderCard={this.renderCard}
                  onSwipedAll={this.onSwipedAllCards}
                  stackSize={2}
                  stackSeparation={15}
                  overlayLabels={{
                    left: {
                      title: 'Delete',
                      style: {
                        label: styles.leftOverlay,
                        wrapper: styles.leftOverlayWrapper,
                      },
                    },
                    right: {
                      title: 'Favorite',
                      style: {
                        label: styles.rightOverlay,
                        wrapper: styles.rightOverlayWrapper,
                      },
                    },
                  }}
                  animateOverlayLabelsOpacity
                  animateCardOpacity
                  swipeBackCard>
                </Swiper>
              )
          ) : null}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
  },
  infoTextWrapper: { flex: 1, alignItems: 'center', justifyContent: 'center' },
  infoText: { fontSize: 18, color: COLORS.infoText },
  upperBackground: {
    height: '25%',
    backgroundColor: COLORS.backgroundProfile,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  swiperWrapper: {
    position: 'absolute',
    flex: 1,
    top: 50,
    left: 0,
    bottom: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: "center",
  },
  headerText: {
    padding: 10,
    paddingLeft: 20,
    color: COLORS.lightTextColor,
    fontSize: 18,
  },
  leftOverlay: {
    backgroundColor: COLORS.danger,
    borderColor: COLORS.danger,
    color: COLORS.lightBackground,
    borderWidth: 1,
    fontSize: 20,
    paddingHorizontal: 20,
  },
  leftOverlayWrapper: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    marginTop: 20,
    marginLeft: -20,
    elevation: 4,
  },
  rightOverlayWrapper: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 20,
    marginLeft: 20,
    elevation: 4,
  },
  rightOverlay: {
    backgroundColor: COLORS.success,
    borderColor: COLORS.success,
    color: COLORS.lightTextColor,
    borderWidth: 1,
    fontSize: 20,
    paddingHorizontal: 20,
  },
});

export default Favorites;
