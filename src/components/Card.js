import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import IconFA from 'react-native-vector-icons/FontAwesome';
import IconEN from 'react-native-vector-icons/Entypo';
import IconFT from 'react-native-vector-icons/Feather';
import { COLORS } from '../utils/Constants';

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      infoTabIndex: 0,
    };
  }

  infoTabPressed = infoTabIndex => {
    if (this.state.infoTabIndex != infoTabIndex) {
      this.setState({ infoTabIndex });
    }
  };
  renderInfoTab = () => {
    const { card } = this.props;
    let d = new Date(card.dob * 1000);
    let dob = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear();
    switch (this.state.infoTabIndex) {
      case 0:
        return (
          <View style={styles.infoWrapper}>
            <Text style={styles.infoTextPrimary}>
              {card.name.first + ' ' + card.name.last}
            </Text>
            <Text style={styles.infoTextSecondary}>{dob}</Text>
          </View>
        );
      case 1:
        return (
          <View style={styles.infoWrapper}>
            <Text style={styles.infoTextPrimary}>
              {card.location.street +
                '\n' +
                card.location.city +
                '-' +
                card.location.zip +
                '\n' +
                card.location.state}
            </Text>
          </View>
        );
      case 2:
        return (
          <View style={styles.infoWrapper}>
            <Text style={styles.infoTextPrimary}>{card.phone}</Text>
            <Text style={{ fontSize: 22, color: 'grey', fontWeight: 'bold' }}>
              {card.cell}
            </Text>
          </View>
        );
      default:
        return null;
    }
  };

  render() {
    const { card, index } = this.props;
    if (card)
      return (
        <View style={styles.card}>
          <View style={styles.imageWrapper}>
            <View style={styles.imageView}>
              <Image source={{ uri: card.picture }} style={styles.image} />
            </View>
          </View>
          <View style={styles.topSection} />
          <View style={styles.bottomSection}>
            <View style={styles.infoSectionWrapper}>
              {this.renderInfoTab()}
              <View style={styles.tabWrapper}>
                <TouchableOpacity
                  style={[
                    styles.tabButton,
                    {
                      borderTopWidth: this.state.infoTabIndex == 0 ? 1.5 : 0,
                    },
                  ]}
                  activeOpacity={1}
                  onPress={() => this.infoTabPressed(0)}>
                  <IconFA
                    color={
                      this.state.infoTabIndex == 0
                        ? COLORS.success
                        : COLORS.infoText
                    }
                    name={'user-o'}
                    size={30}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={[
                    styles.tabButton,
                    {
                      borderTopWidth: this.state.infoTabIndex == 1 ? 1.5 : 0,
                    },
                  ]}
                  activeOpacity={1}
                  onPress={() => this.infoTabPressed(1)}>
                  <IconEN
                    color={
                      this.state.infoTabIndex == 1
                        ? COLORS.success
                        : COLORS.infoText
                    }
                    name={'location'}
                    size={30}
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={[
                    styles.tabButton,
                    {
                      borderTopWidth: this.state.infoTabIndex == 2 ? 1.5 : 0,
                    },
                  ]}
                  activeOpacity={1}
                  onPress={() => this.infoTabPressed(2)}>
                  <IconFT
                    color={
                      this.state.infoTabIndex == 2
                        ? COLORS.success
                        : COLORS.infoText
                    }
                    name={'phone'}
                    size={30}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      );
    else return null;
  }
}
const styles = StyleSheet.create({
  card: {
    flex: 1,
    borderRadius: 4,
    justifyContent: 'center',
    backgroundColor: COLORS.lightBackground,
    elevation: 3,
    marginBottom: 100,
    shadowColor: COLORS.infoText,
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 4,
    shadowOpacity: 1.0
  },
  infoWrapper: {
    paddingVertical: 50,
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  infoTextPrimary: {
    fontSize: 22,
    marginVertical: 10,
    fontWeight: 'bold',
    color: COLORS.infoText,
  },
  infoTextSecondary: { fontSize: 20, color: COLORS.infoText },
  imageWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1,
    width: '100%',
    alignItems: 'center',
  },
  imageView: {
    height: 180,
    width: 180,
    borderRadius: 90,
    borderWidth: 0.4,
    transform: [{ translateY: 60 }],
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.lightBackground,
  },
  image: {
    height: 170,
    width: 170,
    borderRadius: 170 / 2,
  },
  topSection: {
    flex: 2,
    backgroundColor: COLORS.cardBack,
    borderBottomWidth: 0.4,
  },
  bottomSection: {
    flex: 5,
    backgroundColor: COLORS.lightBackground,
  },
  infoSectionWrapper: { justifyContent: 'flex-end', flex: 1 },
  tabWrapper: { flexDirection: 'row', justifyContent: 'center' },
  tabButton: {
    padding: 20,
    borderTopColor: COLORS.success,
  },
});

export default Card;
