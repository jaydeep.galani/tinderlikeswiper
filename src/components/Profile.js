import React, { Component } from 'react';
import Swiper from 'react-native-deck-swiper';
import { StyleSheet, Text, View, Image, StatusBar } from 'react-native';
import { connect } from 'react-redux';
import { randomPeopleDataAction } from '../actions/HomeActions';
import Card from './Card';
import IconEN from 'react-native-vector-icons/Entypo';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { addProfile, removeAllProfiles } from '../utils/DBUtils';
import { COLORS } from '../utils/Constants';
class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      peopleData: [],
      swipedAllCards: false,
      swipeDirection: '',
      cardIndex: 0,
      isFirstDataFetched: false,
      swiperKey: 0,
    };
  }

  componentDidMount = () => {
    // removeAllProfiles();
    this.getProfiles();
  };

  renderCard = (card, index) => {
    return <Card card={card} index={index} />;
  };

  onSwiped = async type => {
    if (type == 'right') {
      let data = this.state.peopleData[this.state.cardIndex];
      addProfile(JSON.parse(JSON.stringify(data)));
    }
    this.setState({ cardIndex: this.state.cardIndex + 1 }, () => {
      if (this.state.cardIndex % 5 == 2) {
        this.getProfiles();
      }
    });
  };

  onSwipedAllCards = () => {
    this.getProfiles();
    this.setState({
      swipedAllCards: true,
    });
  };

  swipeLeft = () => {
    this.swiper.swipeLeft();
  };

  getProfiles = () => {
    let numberOfProfiles = 5;
    for (let i = 0; i < numberOfProfiles; i++) {
      setTimeout(() => {
        this.props.randomPeopleDataAction();
      }, 10);
    }
  };

  componentDidUpdate = prevProps => {
    if (this.props.peopleData != prevProps.peopleData) {
      let newProfile = this.props.peopleData.results[0].user;
      this.setState(
        {
          peopleData: [...this.state.peopleData, newProfile],
          swiperKey: this.state.swiperKey + 1,
          isFirstDataFetched: true,
        },
        () => {
          this.setState({ swipedAllCards: false });
        },
      );
    }
  };

  render() {
    return (
      <View key={this.state.swiperKey} style={styles.container}>
        <StatusBar backgroundColor={COLORS.backgroundProfile} />
        <View style={styles.upperBackground}>
          <Text onPress={() => { alert("hi") }} style={styles.headerText}>{'Profiles'}</Text>

          <TouchableOpacity
            style={{ padding: 10, zIndex: 10, }}
            onPress={() => {
              console.log("=======navigation")
              this.props.navigation.navigate('Favorites');
            }}>
            <IconEN
              name={'star-outlined'}
              color={COLORS.lightTextColor}
              size={25}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.swiperWrapper}>
          {this.state.isFirstDataFetched ? (
            this.state.swipedAllCards ? null : (
              <Swiper
                ref={swiper => {
                  this.swiper = swiper;
                }}
                backgroundColor={'transparent'}
                onSwiped={() => this.onSwiped('general')}
                onSwipedLeft={() => this.onSwiped('left')}
                onSwipedRight={() => this.onSwiped('right')}
                onSwipedTop={() => this.onSwiped('top')}
                onSwipedBottom={() => this.onSwiped('bottom')}
                cards={this.state.peopleData}
                cardIndex={this.state.cardIndex}
                cardVerticalMargin={50}
                renderCard={this.renderCard}
                onSwipedAll={this.onSwipedAllCards}
                stackSize={2}
                stackSeparation={15}
                overlayLabels={{
                  left: {
                    title: 'Skip',
                    style: {
                      label: styles.leftOverlay,
                      wrapper: styles.leftOverlayWrapper,
                    },
                  },
                  right: {
                    title: 'Like',
                    style: {
                      label: styles.rightOverlay,
                      wrapper: styles.rightOverlayWrapper,
                    },
                  },
                }}
                animateOverlayLabelsOpacity
                animateCardOpacity
                swipeBackCard></Swiper>
            )
          ) : (
              // <View style={styles.infoTextWrapper}>
              <Text style={styles.infoText}>{'Loading...'}</Text>
              // </View>
            )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.backgroundDefault,
  },
  infoTextWrapper: { flex: 1, alignItems: 'center', justifyContent: 'center' },
  infoText: { fontSize: 18, color: COLORS.infoText },
  upperBackground: {
    height: '25%',
    backgroundColor: COLORS.backgroundProfile,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  swiperWrapper: {
    position: 'absolute',
    flex: 1,
    top: 50,
    left: 0,
    bottom: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: "center",
  },
  headerText: {
    padding: 10,
    paddingLeft: 20,
    color: COLORS.lightTextColor,
    fontSize: 18,
  },
  leftOverlay: {
    backgroundColor: COLORS.danger,
    borderColor: COLORS.danger,
    color: COLORS.lightBackground,
    borderWidth: 1,
    fontSize: 20,
    paddingHorizontal: 20,
  },
  leftOverlayWrapper: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    marginTop: 20,
    marginLeft: -20,
    elevation: 4,
  },
  rightOverlayWrapper: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 20,
    marginLeft: 20,
    elevation: 4,
  },
  rightOverlay: {
    backgroundColor: COLORS.success,
    borderColor: COLORS.success,
    color: COLORS.lightTextColor,
    borderWidth: 1,
    fontSize: 20,
    paddingHorizontal: 20,
  },
});

const mapStateToProps = ({ HomeReducer }) => {
  const { peopleData } = HomeReducer;
  return { peopleData };
};

const mapDispatchToProps = {
  randomPeopleDataAction,
};
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
