import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {connect} from 'react-redux';
import {randomPeopleDataAction} from '../actions/HomeActions';
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      peopleData: [],
    };
  }

  componentDidUpdate = prevProps => {
    if (this.props.peopleData != prevProps.peopleData) {
      let newProfile = this.props.peopleData;
      this.setState({peopleData: [...this.state.peopleData, newProfile]});
    }
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: '#F5FCFF',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          onPress={() => {
            this.props.navigation.navigate('Profile');
          }}>
          {'Go to Profile'}
        </Text>
      </View>
    );
  }
}

const mapStateToProps = ({HomeReducer}) => {
  const {peopleData} = HomeReducer;
  return {peopleData};
};

const mapDispatchToProps = {
  randomPeopleDataAction,
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);
